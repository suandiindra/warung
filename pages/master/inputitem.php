<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel" style="padding-top:10px">
            <div class="x_title" >
                <h2>Master Item</h2>
                <div class="clearfix float-right" style="margin-top:10px">
                    <a href="./?go=items" class="text-primary">Data Item</a>
                </div>
            </div>
            <div class="x_content">
                <div class="form-horizontal form-label-left">
                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Nama Item <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                            <input type="text" id="namaitem" required="required" class="form-control ">
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="last-name">Harga <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                            <input type="text" id="harga" name="harga" required="required" class="form-control">
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="last-name">Tipe
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                            <select class="form-control" id="tipejual">
                                <option></option>
                                <option value="1">Jualan</option>
                                <option value="0">Non Jualan</option>
                            </select>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="last-name">Modal
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                            <input type="text" id="modal" name="modal" class="form-control">
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="last-name">Keuntungan
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                            <input type="text" id="keuntungan" name="keuntungan" class="form-control">
                        </div>
                    </div>
                    <div class="item form-group" style="margin-top:10px">
                        <div class="col-md-6 col-sm-6 ">
                            <Button class="btn btn-success" onClick="proses()">Simpan</Button>
                        </div>
                    </div>
                    <div class="ln_solid"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    window.addEventListener('load', lihat, false);

    function lihat(){
        console.log('okeee...');
    }
    function proses(){
        let namaitem = $("#namaitem").val()
        let harga = $("#harga").val()
        let tipejual = $("#tipejual").val()
        let modal = $("#modal").val()
        let keuntungan = $("#keuntungan").val()

        console.log(namaitem,harga,keuntungan,tipejual);
        $.ajax({
            type:'POST',
            url:'pages/master/master_be.php',
            data:{
                proses:true,
                namaitem : namaitem,
                harga : harga,
                tipejual : tipejual,
                modal : modal,
                keuntungan : keuntungan
            },
            success: function(data){
                console.log(data);
                if(data == "200"){
                    window.location = "./?go=items"
                }
            }
        })
    }
</script>