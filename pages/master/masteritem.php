<div class="x_content">
    <div class="row">
        <div class="col-sm-12">
        <div class="card-box table-responsive">
            <table id="tbItem" class="table table-striped table-bordered" style="width:100%">
                <thead>
                <tr>
                    <td class="text-center"><i class="fa fa-gear"></i></td>
                    <th>Item</th>
                    <th>Harga</th>
                    <th>Modal</th>
                    <th>Kategory</th>
                    <th>Keuntungan</th>
                </tr>
                </thead>
                <tbody id="dtlItem">
                </tbody>
            </table>
        </div>
        </div>
    </div>
    <!-- modal -->
    <div class="modal fade" id="modalItem" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Item Detail</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="mb-3">
                <label for="disabledTextInput" class="form-label">Nama Produk</label>
                <input type="text" id="nama_produk" class="form-control">
            </div>
            <div class="mb-3">
                <label for="disabledTextInput" class="form-label">Harga</label>
                <input type="text" id="harga" class="form-control">
            </div>
            <div class="mb-3">
                <label for="disabledTextInput" class="form-label">Modal</label>
                <input type="text" id="modal" class="form-control">
            </div>
            <div class="mb-3">
                <label for="disabledTextInput" class="form-label">Kuntungan</label>
                <input type="text" id="keuntungan" class="form-control">
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" onClick="edit()">Save changes</button>
        </div>
        </div>
    </div>
    </div>
</div>
<script>
    window.addEventListener('load', lihat, false);
    let v_id = ``
    function lihat(){
        var table = $('#tbItem').DataTable();
        table.clear().destroy(); 
        $.ajax({
            type:'POST',
            url:'pages/master/master_be.php',
            data:{
                find:true,
            },
            success: function(data){
                let objek = $.parseJSON(data)
                let datas = objek.data
                let dom = ``
                for(let i = 0; i<datas.length; i++){
                    dom = dom + `<tr>
                        <td class="text-center"><i class="fa fa-external-link" onClick="openModal('${datas[i].m_item_id}')"></i></td>
                        <td>${datas[i].nama_item}</td>
                        <td>${duit(datas[i].harga)}</td>
                        <td>${duit(datas[i].modal)}</td>
                        <td>${datas[i].isjual}</td>
                        <td>${duit(datas[i].keuntungan)}</td>
                    </tr>`
                }
                $(`#dtlItem`).html(dom)
                $('#tbItem').DataTable(({ 
                    "destroy": true, //use for reinitialize datatable
                }));
            }
        })
    }

    function duit(v){
        var 	bilangan = v;
        var	reverse = bilangan.toString().split('').reverse().join(''),
            ribuan 	= reverse.match(/\d{1,3}/g);
            ribuan	= ribuan.join('.').split('').reverse().join('');
        return ribuan
    }
    function openModal(id){
        // console.log(id);
        $.ajax({
            type:'POST',
            url:'pages/master/master_be.php',
            data:{
                findOne:true,
                id : id
            },
            success: function(data){
                let objek = $.parseJSON(data)
                let datas = objek.data
                // console.log(datas);
                v_id = id
                $('#nama_produk').val(datas[0].nama_item)
                $('#harga').val(datas[0].harga)
                $('#keuntungan').val(datas[0].keuntungan)
                $('#modal').val(datas[0].modal)
                $('#modalItem').modal('show')
            }
        })
    }
    function edit(){
        $.ajax({
            type:'POST',
            url:'pages/master/master_be.php',
            data:{
                edit:true,
                id : v_id,
                nama_produk : $('#nama_produk').val(),
                harga : $('#harga').val(),
                keuntungan : $('#keuntungan').val(),
                modal : $('#modal').val(),
            },
            success: function(data){
               console.log(data);
               if(data == "200"){
                    $('#modalItem').modal('toggle');
                    lihat()
               }else if(data == "500"){
               }
            }
        })
    }
</script>