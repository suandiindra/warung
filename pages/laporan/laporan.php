<div class="x_content">
    <div class="row">
        <!-- --- -->
        <div class="col col-md-5" style="background-color:#6ab04c; border-radius:10px; margin:10px">
            <div class="row" style="display: inline-block;" >
                <div class="col col-md-12">
                    <div class="row">
                        <div class="col-md-12 bg-warning text-white" style="padding: 10px;">
                            Keuntungan
                        </div>
                    </div>
                    <div class="row">
                        <div class="col text-white">
                            <p style="font-size:20px"><b id="untung">0</b></p>
                            keuntungan hari ini
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col col-md-5" style="background-color:#0984e3; border-radius:10px; margin:10px">
            <div class="row" style="display: inline-block;" >
                <div class="col col-md-12">
                    <div class="row">
                        <div class="col-md-12 bg-warning text-white" style="padding: 10px;">
                            Modal
                        </div>
                    </div>
                    <div class="row">
                        <div class="col text-white">
                            <p style="font-size:20px"><b id="modal">0</b></p>
                            modal hari ini
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col col-md-12" style="background-color:#ff6b6b; border-radius:10px; margin:10px">
            <div class="row" style="display: inline-block;" >
                <div class="col col-md-12">
                    <div class="row">
                        <div class="col-md-12 text-white" style="padding: 10px; background-color:#6ab04c">
                            Total Keuntungan
                        </div>
                    </div>
                    <div class="row">
                        <div class="col text-white">
                            <p style="font-size:20px"><b id="untungBulanan">0</b></p>
                            Total Keuntungan Bulan ini
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col col-md-12" style="background-color:#ffb142; border-radius:10px; margin:10px">
            <div class="row" style="display: inline-block;" >
                <div class="col col-md-12">
                    <div class="row">
                        <div class="col-md-12 text-white" style="padding: 10px; background-color:#6ab04c">
                            Total Modal
                        </div>
                    </div>
                    <div class="row">
                        <div class="col text-white">
                            <p style="font-size:20px"><b id="modalBulanan">0</b></p>
                            Total Modal Bulan ini
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    window.addEventListener('load', lihat, false);
    function lihat(){
        // console.log('asd');
        $.ajax({
            type:'POST',
            url:'pages/laporan/laporan_be.php',
            data:{
                find:true
            },
            success: function(data){
                // console.log(data);
                var objek_bulanan = $.parseJSON(data).data_bulanan
                var objek = $.parseJSON(data).data
                console.log(objek_bulanan);
                let modal = 0
                let untung = 0

                let modalBulanan = 0
                let untungBulanan = 0

                for(let i = 0; i<objek.length;i++){
                    modal = modal + parseFloat(objek[i].total_modal)
                    untung = untung + parseFloat(objek[i].total_keuntungan)
                }
                $(`#untung`).html(duit(untung))
                $(`#modal`).html(duit(modal))

                for(let i = 0; i<objek_bulanan.length;i++){
                    modalBulanan = modalBulanan + parseFloat(objek_bulanan[i].total_modal)
                    untungBulanan = untungBulanan + parseFloat(objek_bulanan[i].total_keuntungan)
                }
                $(`#untungBulanan`).html(duit(untungBulanan))
                $(`#modalBulanan`).html(duit(modalBulanan))
                
            }
        })
    }

    function duit(v){
        var 	bilangan = v;
        var	reverse = bilangan.toString().split('').reverse().join(''),
            ribuan 	= reverse.match(/\d{1,3}/g);
            ribuan	= ribuan.join('.').split('').reverse().join('');
        return ribuan
    }
</script>