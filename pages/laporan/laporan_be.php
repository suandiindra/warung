<?php
    include('../../config/config.php');
    include('../../utils/fungsi.php');
    $obj = new stdClass();

    if(isset($_POST['find'])){
        $sel1 = "select b.*,total_harga-total_keuntungan as total_modal from transaksi_masuk tm 
            inner join transaksi_masuk_detail b on b.transaksi_masuk_id = tm.transaksi_masuk_id 
            where tm.isactive = 1 and b.isactive = 1
            and catatan = date_format(DATE_ADD(now(),INTERVAL -1 day),'%Y-%m-%d')";

        $data1 = json_decode(queryJson($con,$sel1),true)['data'];

        $sel2 = "select b.*,total_harga-total_keuntungan as total_modal from transaksi_masuk tm 
            inner join transaksi_masuk_detail b on b.transaksi_masuk_id = tm.transaksi_masuk_id 
            where tm.isactive = 1 and b.isactive = 1
            and date_format(catatan,'%Y-%m') = date_format(DATE_ADD(now(),INTERVAL -0 day),'%Y-%m')";

        $data1 = json_decode(queryJson($con,$sel1),true)['data'];
        $data2 = json_decode(queryJson($con,$sel2),true)['data'];

        $obj -> data = $data1;  
        $obj -> data_bulanan = $data2;  
        $data = json_encode($obj);
        echo $data;

        // echo $sel1;
    }
?>