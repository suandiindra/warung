<div class="x_content">
    <Button class="btn btn-success" onclick="manageBalance()">Manage</Button>
    <div class="row">
        <div class="col-sm-12">
        <div class="card-box table-responsive">
            <table id="tbBalance" class="table table-striped table-bordered" style="width:100%">
                <thead>
                <tr>
                    <td class="text-center"><i class="fa fa-gear"></i></td>
                    <th>Periode</th>
                    <th>Saldo Awal</th>
                    <th>Debit</th>
                    <th>Kredit</th>
                    <th>Balance</th>
                </tr>
                </thead>
                <tbody id="dtBalance">
                </tbody>
            </table>
        </div>
        </div>
    </div>
    <!-- modal -->
    <div class="modal fade" id="modalItem" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Manage Balance</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="mb-3">
                <label for="disabledTextInput" class="form-label">Periode</label>
                <input type="date" id="periode" class="form-control">
            </div>
            <div class="mb-3">
                <label for="disabledTextInput" class="form-label">Saldo Awal</label>
                <input type="number" id="amount" class="form-control">
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" onClick="save()">Save</button>
        </div>
        </div>
    </div>
    </div>
</div>
<script>
    window.addEventListener('load', lihat, false);
    function lihat(){
        var table = $('#tbBalance').DataTable();
        $.ajax({
            type:'POST',
            url:'pages/balance/balance_be.php',
            data:{
                find:true,
            },
            success: function(data){
                let objek = $.parseJSON(data)
                let datas = objek.data
                let dom = ``
                for(let i = 0; i<datas.length; i++){
                    dom = dom + `<tr>
                        <td class="text-center"><i class="fa fa-external-link" onClick="openModal('${datas[i].m_balance_id}')"></i></td>
                        <td>${datas[i].per}</td>
                        <td>${duit(datas[i].amount)}</td>
                        <td>${duit(0)}</td>
                        <td>${0}</td>
                        <td>${duit(datas[i].amount)}</td>
                    </tr>`
                }
                console.log(dom);

                $(`#dtBalance`).html(dom)
                // $('#tbBalance').DataTable(({ 
                //     "destroy": true, //use for reinitialize datatable
                // }));
            }
        })
    }
    function duit(v){
        var 	bilangan = v;
        var	reverse = bilangan.toString().split('').reverse().join(''),
            ribuan 	= reverse.match(/\d{1,3}/g);
            ribuan	= ribuan.join('.').split('').reverse().join('');
        return ribuan
    }
    function manageBalance(){
        $('#modalItem').modal('show')
    }
    function save(){
        $.ajax({
            type:'POST',
            url:'pages/balance/balance_be.php',
            data:{
                proses:true,
                periode : $('#periode').val(),
                amount : $('#amount').val(),
            },
            success: function(data){
               console.log(data);
               if(data == "200"){
                    $('#periode').val("")
                    $('#amount').val("")
                    $('#modalItem').modal('toggle');
                    lihat()
               }else if(data == "500"){
                    alert(`Balance sudah pernah di set sebelumnya`)
                    $('#modalItem').modal('toggle');
               }
            }
        })
    }
</script>