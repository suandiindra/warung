<?php
    include('../../config/config.php');
    include('../../utils/fungsi.php');

    if(isset($_POST['find'])){
        $tgl1 = $_POST['tgl1'];
        $tgl2 = $_POST['tgl2'];
        $filter = " and date_format(tm.catatan ,'%Y-%m-%d') = date_format(DATE_ADD(now(),INTERVAL -1 day),'%Y-%m-%d') ";
        if(strlen($tgl1) > 0){
            $filter = " and date_format(tm.catatan ,'%Y-%m-%d') between '$tgl1' and '$tgl2' ";
        }
        $sel = "select date_format(tm.catatan ,'%Y-%m-%d') as period
                ,b.nama_item ,tm.channel ,b.harga ,b.qty ,b.total_harga
                ,b.total_keuntungan,tm.transaksi_masuk_id ,transaksi_masuk_detail_id
                from transaksi_masuk tm 
                inner join transaksi_masuk_detail b on b.transaksi_masuk_id = tm.transaksi_masuk_id 
                where tm.isactive  = 1 and b.isactive = 1 $filter
                order by date_format(tm.catatan ,'%Y-%m-%d') ,channel";
        echo queryJSON($con,$sel);
        // echo $sel;
    }
    if(isset($_POST['findOne'])){
        $id = $_POST['id'];
        $sel = "select date_format(tm.createddate ,'%Y-%m-%d') as period
                ,b.nama_item ,tm.channel ,b.harga ,b.qty ,b.total_harga
                ,b.total_keuntungan,b.transaksi_masuk_detail_id,b.m_item_id 
                from transaksi_masuk tm 
                inner join transaksi_masuk_detail b on b.transaksi_masuk_id = tm.transaksi_masuk_id 
                where tm.isactive  = 1 and transaksi_masuk_detail_id = '$id'
                order by date_format(tm.createddate ,'%Y-%m-%d') ,channel";
        echo queryJSON($con,$sel);
        // echo $sel;
    }
    if(isset($_POST['proses'])){
        $tanggal = $_POST['tanggal'];
        $produk = $_POST['produk'];
        $harga = $_POST['harga'];
        $qty = $_POST['qty'];
        $channel = $_POST['channel'];

        $sel1 = "select * from m_item where m_item_id = '$produk'";
        $res = mysqli_query($con,$sel1);
        $ro = mysqli_fetch_array($res);

        $sel2 = "select uuid() as unik";
        $res2 = mysqli_query($con,$sel2);
        $ro2 = mysqli_fetch_array($res2);

        $unik = $ro2['unik'];
        $nama_produk = $ro['nama_item'];
        $modal = $ro['modal'];
        $keuntungan = $ro['keuntungan'];

        $total_harga = $qty * $harga;
        $total_untung = $qty * $keuntungan;

        $insertH = "insert into transaksi_masuk (transaksi_masuk_id,createddate,channel,isactive,catatan)
        values ('$unik',now(),'$channel',1,'$tanggal')";
        
        $res1 = mysqli_query($con,$insertH);

        if($res1){
            $insertD = "insert into transaksi_masuk_detail
            (transaksi_masuk_detail_id,transaksi_masuk_id ,m_item_id ,nama_item ,harga ,qty ,keuntungan 
            ,isactive ,createddate,modal,total_harga,total_keuntungan )
            values (uuid(),'$unik' ,'$produk' ,'$nama_produk' ,'$harga' ,$qty ,$keuntungan ,1 ,now(),'$modal'
            ,'$total_harga','$total_untung' )";

            $res2 = mysqli_query($con,$insertD);
            if($res2){
                echo "200";
            }else{
                echo $insertD;
            }
        }else{
            echo $insertH;
        }
        
        
        
    }
    if(isset($_POST['editObj'])){
        $id = $_POST['id'];
        $period = $_POST['period'];
        $produk = $_POST['produk'];
        $qty = $_POST['qty'];
        $channel = $_POST['channel'];

        $sel = "select * from m_item mi where m_item_id = '$produk'";
        $res = mysqli_query($con,$sel);
        $dt = mysqli_fetch_array($res);
        $item = $dt['nama_item'];
        $harga = $dt['harga'];
        $keuntungan = $dt['keuntungan'];
        $modal = $dt['modal'];

        $total_harga = $qty * $harga;
        $total_untung = $qty * $keuntungan; 



        $upd = "update transaksi_masuk_detail set m_item_id = '$produk', nama_item = '$item' 
        , qty = '$qty', harga = '$harga' ,keuntungan = '$keuntungan',total_keuntungan = '$total_untung'
        , total_harga = '$total_harga', modal = '$modal'
        where transaksi_masuk_detail_id  = '$id'";

        $res = mysqli_query($con,$upd);

        if($res){
            echo "200";
        }else{
            echo $upd;
        }
        // echo $dt['nama_item'];
    }
?>