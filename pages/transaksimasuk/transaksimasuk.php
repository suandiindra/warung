<div class="x_content">
    <div class="row">
        <div class="col col-md-4">
            <input type="date" id="tgl1" class="form-control"/>
        </div>
        <div class="col col-md-4">
            <input type="date" id="tgl2" class="form-control"/>
        </div>
        <div class="col col-md-2" style="margin-top:10px">
            <Button class="btn btn-success col-md-12" onclick="buka()">Tambah Transaksi</Button>
        </div>
        <div class="col col-md-2" style="margin-top:10px">
            <Button class="btn btn-danger col-md-12" onclick="lihat()">Lihat</Button>
        </div>
    </div>
    <div class="row">
        <!-- --- -->
        <div class="col col-md-5" style="background-color:#ffff; border-radius:10px">
        <div class="row" style="display: inline-block;" >
          <div class="tile_count">
            <div class="col-md-12 col-sm-4  tile_stats_count">
              <span class="count_top"><i class="fa fa-calculator"></i> Total Penjualan</span>
              <div class="count" id="jumlah"></div>
              <span class="count_bottom text-success">Penjualan/porsi</span>
            </div>
          </div>
        </div>
        </div>
        <div class="col-md-2"></div>
        <div class="col col-md-5" style="background-color:#ffff; border-radius:10px">
        <div class="row" style="display: inline-block;" >
          <div class="tile_count">
            <div class="col-md-12 col-sm-4  tile_stats_count">
              <span class="count_top"><i class="fa fa-cc"></i> Total Rupiah</span>
              <div class="count" id="amount"></div>
              <span class="count_bottom">From last Week</span>
            </div>
          </div>
        </div>
        </div>
        <!-- --- -->
    </div>
    <div class="row">
        <div class="col-sm-12">
        <div class="card-box table-responsive">
            <table id="tbTransaksi" class="table table-striped table-bordered" style="width:100%">
                <thead>
                <tr>
                    <td class="text-center"><i class="fa fa-gear"></i></td>
                    <th>Tanggal</th>
                    <th>Item</th>
                    <th>Channel</th>
                    <th>Harga</th>
                    <th>Qty</th>
                    <th>Biaya</th>
                </tr>
                </thead>
                <tbody id="dtTransaksi">
                </tbody>
            </table>
        </div>
        </div>
    </div>
    <!-- modal -->
    <div class="modal fade" id="modalTrans" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Tambah Transaksi</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="mb-3">
                <label for="disabledTextInput" class="form-label">Priode</label>
                <input type="date" id="tanggal" class="form-control">
            </div>
            <div class="mb-3">
                <label for="disabledTextInput" class="form-label">Pilih Produk</label>
                <select class="form-control" id="produk" onchange="lihatHarga(this)">
                    <option></option>
                <?php
                    $sel = "select * from m_item mi where isjual = 1 and isactive = 1 order by nama_item asc";
                    $res = mysqli_query($con,$sel);
                    while($da = mysqli_fetch_array($res)){
                ?>
                    <option value="<?php echo $da['m_item_id']; ?>"><?php echo $da['nama_item'] ?></option>
                <?php
                    }
                ?>
                </select>
            </div>
            <div class="mb-3">
                <label for="disabledTextInput" class="form-label">Harga</label>
                <input type="text" id="harga" readonly class="form-control">
            </div>
            <div class="mb-3">
                <label for="disabledTextInput" class="form-label">QTY</label>
                <input type="text" id="qty" class="form-control">
            </div>
            <div class="mb-3">
                <label for="disabledTextInput" class="form-label">Channel</label>
                <select class="form-control" id="channel">
                    <option value="GoFood">GoFood</option>
                    <option value="GrabFood">GrabFood</option>
                    <option value="Shopee Food">Shopee Food</option>
                    <option value="Kulina">Kulina</option>
                    <option value="Traveloka">Traveloka</option>
                </select>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" onClick="tambahData()">Tambah</button>
        </div>
        </div>
    </div>
    </div>
    <!-- modal edit-->
    <div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Edit Transaksi</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="mb-3">
                <label for="disabledTextInput" class="form-label">Priode</label>
                <input type="date" id="tanggal_edit" class="form-control">
            </div>
            <div class="mb-3">
                <label for="disabledTextInput" class="form-label">Pilih Produk</label>
                <select class="form-control" id="produk_edit" onchange="lihatHarga(this)">
                    <option></option>
                <?php
                    $sel = "select * from m_item mi where isjual = 1 and isactive = 1 order by nama_item asc";
                    $res = mysqli_query($con,$sel);
                    while($da = mysqli_fetch_array($res)){
                ?>
                    <option value="<?php echo $da['m_item_id']; ?>"><?php echo $da['nama_item'] ?></option>
                <?php
                    }
                ?>
                </select>
            </div>
            <div class="mb-3">
                <label for="disabledTextInput" class="form-label">Harga</label>
                <input type="text" id="harga_edit" readonly class="form-control">
            </div>
            <div class="mb-3">
                <label for="disabledTextInput" class="form-label">QTY</label>
                <input type="text" id="qty_edit" class="form-control">
            </div>
            <div class="mb-3" style="display:none">
                <label for="disabledTextInput" class="form-label">Channel</label>
                <select class="form-control" id="channel_edit">
                    <option value="GoFood">GoFood</option>
                    <option value="GrabFood">GrabFood</option>
                    <option value="Shopee Food">Shopee Food</option>
                    <option value="Kulina">Kulina</option>
                    <option value="Traveloka">Traveloka</option>
                </select>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" onClick="editData()">Edit Data</button>
            <button type="button" class="btn btn-danger" onClick="hapusData()">Hapus</button>
        </div>
        </div>
    </div>
    </div>
</div>
<script>
    window.addEventListener('load', lihat, false);
    var key_id = ``
    function lihat(){
        // console.log('lihat');
        key_id = ``
        var table = $('#tbTransaksi').DataTable();
        table.clear().destroy(); 
        $.ajax({
            type:'POST',
            url:'pages/transaksimasuk/transaksimasuk_be.php',
            data:{
                find:true,
                tgl1 : $(`#tgl1`).val(),
                tgl2 : $(`#tgl2`).val()
            },
            success: function(data){
                // console.log(data);
                let objek = $.parseJSON(data)
                let datas = objek.data
                console.log(datas);
                let dom = ``
                let jml = 0
                let amount = 0
                for(let i = 0; i<datas.length; i++){
                    dom = dom + `<tr>
                        <td class="text-center"><i class="fa fa-external-link" onClick="openTrans('${datas[i].transaksi_masuk_detail_id}')"></i></td>
                        <td>${datas[i].period}</td>
                        <td>${datas[i].nama_item}</td>
                        <td>${datas[i].channel}</td>
                        <td>${duit(datas[i].harga)}</td>
                        <td>${duit(datas[i].qty)}</td>
                        <td>${duit(datas[i].total_harga)}</td>
                    </tr>`
                    jml = jml + parseInt(datas[i].qty)
                    amount = amount + parseInt(datas[i].total_harga)
                }
                $(`#jumlah`).html(duit(jml))
                $(`#amount`).html(duit(amount))
                $(`#dtTransaksi`).html(dom)
                $('#tbTransaksi').DataTable(({ 
                    "destroy": true, //use for reinitialize datatable
                }));
            }
        })
    }
    function buka(){
        $(`#modalTrans`).modal(`show`)
    }
    function editData(){
        let tgl = $(`#tanggal_edit`).val()
        let produk = $(`#produk_edit`).val()
        let qty = $(`#qty_edit`).val()
        let channel = $(`#channel_edit`).val()
        $.ajax({
            type:'POST',
            url:'pages/transaksimasuk/transaksimasuk_be.php',
            data:{
                editObj:true,
                id : key_id,
                period : tgl,
                produk : produk,
                qty : qty,
                channel : channel
            },
            success: function(data){
                console.log(data);
                if(data == "200"){
                    alert("Berhasil Edit")
                    $(`#modalEdit`).modal('toggle')
                    lihat()
                }
                // let objek = JSON.parse(data)
                // objek = objek.data
                // console.log(objek[0]);
            }
        })
    }
    function lihatHarga(v){
        console.log(v);
        if(v.value.length == 0){
            return
        }
        $.ajax({
            type:'POST',
            url:'pages/master/master_be.php',
            data:{
                findOne:true,
                id : v.value
            },
            success: function(data){
                let objek = $.parseJSON(data)
                let datas = objek.data
                $(`#harga`).val(datas[0].harga)
            }
        })
    }
    function tambahData(){
        console.log('mantap');
        let tanggal = $(`#tanggal`).val()
        let produk = $(`#produk`).val()
        let harga = $(`#harga`).val()
        let qty = $(`#qty`).val()
        let channel = $(`#channel`).val()

        $.ajax({
            type:'POST',
            url:'pages/transaksimasuk/transaksimasuk_be.php',
            data:{
                proses:true,
                tanggal : tanggal,
                produk : produk,
                harga : harga,
                qty : qty,
                channel : channel
            },
            success: function(data){
                // console.log(data);
                if(data == "200"){
                    alert(`Berhasil..`)
                    // $(`#tanggal`).val("")
                    $(`#produk`).val("")
                    $(`#harga`).val("")
                    $(`#qty`).val("")
                    // $(`#channel`).val("")
                    lihat()
                }
            }
        })
    }
    function duit(v){
        var 	bilangan = v;
        var	reverse = bilangan.toString().split('').reverse().join(''),
            ribuan 	= reverse.match(/\d{1,3}/g);
            ribuan	= ribuan.join('.').split('').reverse().join('');
        return ribuan
    }
    function openTrans(v){
        key_id = v
        $.ajax({
            type:'POST',
            url:'pages/transaksimasuk/transaksimasuk_be.php',
            data:{
                findOne:true,
                id : v
            },
            success: function(data){
                let objek = JSON.parse(data)
                objek = objek.data
                // console.log(objek[0]);
                obj = objek[0]
                $(`#tanggal_edit`).val(obj.period)
                $(`#produk_edit`).val(obj.m_item_id)
                $(`#harga_edit`).val(duit(obj.harga))
                $(`#qty_edit`).val(obj.qty)
                $(`#channel_edit`).val(obj.channel)
                $(`#modalEdit`).modal('show')
            }
        })
    }
</script>