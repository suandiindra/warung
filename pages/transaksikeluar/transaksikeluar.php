<div class="x_content">
    <div class="row">
        <div class="col col-md-4">
            <input type="date" id="tgl1" class="form-control"/>
        </div>
        <div class="col col-md-4">
            <input type="date" id="tgl2" class="form-control"/>
        </div>
        <div class="col col-md-2" style="margin-top:10px">
            <Button class="btn btn-success col-md-12" onclick="buka()">Tambah Transaksi</Button>
        </div>
        <div class="col col-md-2" style="margin-top:10px">
            <Button class="btn btn-danger col-md-12" onclick="lihat()">Lihat</Button>
        </div>
    </div>
    <div class="row">
        <!-- --- -->
        <div class="col col-md-12" style="background-color:#ffff; border-radius:10px">
        <div class="row" style="display: inline-block;" >
          <div class="tile_count">
            <div class="col-md-12 col-sm-4  tile_stats_count">
              <span class="count_top"><i class="fa fa-calculator"></i> Total Pengeluaran</span>
              <div class="count" id="biaya"></div>
            </div>
          </div>
        </div>
        </div>
        <!-- --- -->
    </div>
    <div class="row">
        <div class="col-sm-12">
        <div class="card-box table-responsive">
            <table id="tbBeli" class="table table-striped table-bordered" style="width:100%">
                <thead>
                <tr>
                    <td class="text-center"><i class="fa fa-gear"></i></td>
                    <th>Tanggal</th>
                    <th>Item</th>
                    <th>Harga</th>
                    <th>Qty</th>
                    <th>Biaya</th>
                </tr>
                </thead>
                <tbody id="dtBeli">
                </tbody>
            </table>
        </div>
        </div>
    </div>
    <!-- modal -->
    <div class="modal fade" id="modalBeli" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Tambah Transaksi</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="mb-3">
                <label for="disabledTextInput" class="form-label">Priode</label>
                <input type="date" id="tanggalbeli" class="form-control">
            </div>
            <div class="mb-3">
                <label for="disabledTextInput" class="form-label">Pilih Produk</label>
                <select class="form-control" id="produkbeli" onchange="lihatHarga(this)">
                    <option></option>
                <?php
                    $sel = "select * from m_item mi where isjual = 0 and isactive = 1 order by nama_item asc";
                    $res = mysqli_query($con,$sel);
                    while($da = mysqli_fetch_array($res)){
                ?>
                    <option value="<?php echo $da['m_item_id']; ?>"><?php echo $da['nama_item'] ?></option>
                <?php
                    }
                ?>
                </select>
            </div>
            <div class="mb-3">
                <label for="disabledTextInput" class="form-label">Harga</label>
                <input type="text" id="hargabeli" readonly class="form-control">
            </div>
            <div class="mb-3">
                <label for="disabledTextInput" class="form-label">QTY</label>
                <input type="text" id="qtybeli" onchange="hitung()" class="form-control">
            </div>
            <div class="mb-3">
                <label for="disabledTextInput" class="form-label">Total</label>
                <input type="text" id="total" readonly class="form-control">
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" id="btnhapus" style="display:none" class="btn btn-danger float-left" data-dismiss="modal" onclick="hapus()">Hapus</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" id="btnproses" onClick="tambahData()">Tambah</button>
        </div>
        </div>
    </div>
    </div>
</div>
<script>  
    window.addEventListener('load', lihat, false);
    var transaksi_keluar_id = ``

    function bukadetail(v){
        transaksi_keluar_id = v
        console.log(transaksi_keluar_id);
        $.ajax({
            type:'POST',
            url:'pages/transaksikeluar/transaksikeluar_be.php',
            data:{
                findOne:true,
                id : transaksi_keluar_id
            },
            success: function(data){
                // console.log(data);
                let objek = $.parseJSON(data)
                let datas = objek.data[0]
                console.log(datas);
                $("#btnhapus"). css("display", "block");
                $(`#tanggalbeli`).val(datas.periode)
                $(`#produkbeli`).val(datas.m_item_id)
                $(`#hargabeli`).val(datas.harga)
                $(`#qtybeli`).val(datas.qty)
                $(`#modalBeli`).modal(`show`)
                // $(`#modalBeli`).modal(`show`)
                $(`#btnproses`).html(`Edit`)
                hitung()
            }
        })

    }
    function hapus(){
        if(!confirm(`Yakin menghapus transaksi..?`)){
            return
        }
        $.ajax({
            type:'POST',
            url:'pages/transaksikeluar/transaksikeluar_be.php',
            data:{
                hapus:true,
                transaksi_keluar_id : transaksi_keluar_id
            },
            success: function(data){
                console.log(data);
                if(data == "200"){
                    alert(`Berhasil..`)
                    transaksi_keluar_id = ``
                    $(`#tanggalbeli`).val("")
                    $(`#produkbeli`).val("")
                    $(`#hargabeli`).val("")
                    $(`#qtybeli`).val("")
                    $(`#total`).val("")
                    lihat()
                    $(`#modalBeli`).modal(`toggle`)
                }
                
            }
        })
    }
    function lihat(){
        var table = $('#tbBeli').DataTable();
        table.clear().destroy(); 
        $.ajax({
            type:'POST',
            url:'pages/transaksikeluar/transaksikeluar_be.php',
            data:{
                find:true,
                tgl1 : $(`#tgl1`).val(),
                tgl2 : $(`#tgl2`).val()
            },
            success: function(data){
                // console.log(data);
                let objek = $.parseJSON(data)
                let datas = objek.data
                // console.log(datas);
                let dom = ``
                let jml = 0
                let amount = 0
                for(let i = 0; i<datas.length; i++){
                    dom = dom + `<tr>
                        <td class="text-center"><i class="fa fa-external-link" onClick="bukadetail('${datas[i].transaksi_keluar_detail_id}')"></i></td>
                        <td>${datas[i].periode}</td>
                        <td>${datas[i].nama_item}</td>
                        <td>${duit(datas[i].harga)}</td>
                        <td>${duit(datas[i].qty)}</td>
                        <td>${duit(datas[i].total)}</td>
                    </tr>`
                    jml = jml + parseInt(datas[i].qty)
                    amount = amount + parseInt(datas[i].total)
                }
                $(`#biaya`).html(duit(amount))
                $(`#dtBeli`).html(dom)
                $('#tbBeli').DataTable(({ 
                    "destroy": true, //use for reinitialize datatable
                }));
            }
        })
    }
    function duit(v){
        var 	bilangan = v;
        var	reverse = bilangan.toString().split('').reverse().join(''),
            ribuan 	= reverse.match(/\d{1,3}/g);
            ribuan	= ribuan.join('.').split('').reverse().join('');
        return ribuan
    }
    function tambahData(){
        let tanggalbeli = $(`#tanggalbeli`).val()
        let produkbeli = $(`#produkbeli`).val()
        let hargabeli = $(`#hargabeli`).val()
        let qtybeli = $(`#qtybeli`).val()
        console.log(transaksi_keluar_id);
        $.ajax({
            type:'POST',
            url:'pages/transaksikeluar/transaksikeluar_be.php',
            data:{
                proses:true,
                tanggalbeli : tanggalbeli,
                produkbeli : produkbeli,
                hargabeli : hargabeli,
                qtybeli : qtybeli,
                transaksi_keluar_id : transaksi_keluar_id
            },
            success: function(data){
                console.log(data);
                if(data == "200"){
                    alert(`Berhasil..`)
                    // $(`#tanggalbeli`).val("")
                    $(`#produkbeli`).val("")
                    $(`#hargabeli`).val("")
                    $(`#qtybeli`).val("")
                    $(`#total`).val("")
                    lihat()
                }
            }
        })
    }
    function buka(){
        transaksi_keluar_id = ``
        $(`#tanggalbeli`).val("")
        $(`#produkbeli`).val("")
        $(`#hargabeli`).val("")
        $(`#qtybeli`).val("")
        $(`#total`).val("")
        $("#btnhapus"). css("display", "none");
        $(`#btnproses`).html(`Tambah`)
        $(`#modalBeli`).modal(`show`)
    }
    function lihatHarga(v){
        console.log(v);
        if(v.value.length == 0){
            return
        }
        $.ajax({
            type:'POST',
            url:'pages/master/master_be.php',
            data:{
                findOne:true,
                id : v.value
            },
            success: function(data){
                let objek = $.parseJSON(data)
                let datas = objek.data
                $(`#hargabeli`).val(datas[0].harga)
                hitung()
            }
        })
    }
    function hitung(){
        try {
            let harga = $(`#hargabeli`).val()
            let qty = $(`#qtybeli`).val()
            let total = parseFloat(harga) * parseFloat(qty)
            total = isNaN(total) ? "0" : total
            $(`#total`).val(total)
        } catch (error) {
            $(`#total`).val("0")
        }
    }
</script>