<?php
    function templatePenawaran(){
        $temp = "<html lang='en'>
            <head>
            <meta charset='utf-8'>
            <meta name='viewport' content='width=device-width, initial-scale=1'>
            <link href='https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css' rel='stylesheet' integrity='sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC' crossorigin='anonymous'>
        
            <title>Hello, world!</title>
            <style>
                th, td {
                    padding: 10px;
                    }
            </style>
            </head>
            <body>
            <h1>Pengajuan PO Customer</h1></br>
            <table style='border:0px !important'>
                <tr>
                    <td>Nomor Dokumen</td>
                    <td>:</td>
                    <td>@dokumen</td>
                </tr>
                <tr>
                    <td>Nama Sales</td>
                    <td>:</td>
                    <td>@sales</td>
                </tr>
                <tr>
                    <td>Customer</td>
                    <td>:</td>
                    <td>@customer</td>
                </tr>
                <tr>
                    <td>Project</td>
                    <td>:</td>
                    <td>@project</td>
                </tr>
                <tr>
                    <td>Jumlah Penawaran</td>
                    <td>:</td>
                    <td>@nominal</td>
                </tr>
                <tr>
                    <td>Suggestion Sistem</td>
                    <td>:</td>
                    <td>@sugest</td>
                </tr>
                <tr>
                    <td>Catatan</td>
                    <td>:</td>
                    <td>@note</td>
                </tr>
            </table>
            <hr>
            <a href='http://employee.ama.id/?go=penawarandetail&id=@keyid'><button style='color:white; background-color: cornflowerblue; border-radius: 5px;'>Periksa</button></a>
            <script src='https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js' integrity='sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM' crossorigin='anonymous'></script>
            </body>
        </html>" ;
        return $temp;
    }
    function templateKlaim(){
        $temp = "<html lang='en'>
                    <head>
                    <meta charset='utf-8'>
                    <meta name='viewport' content='width=device-width, initial-scale=1'>
                    <link href='https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css' rel='stylesheet' integrity='sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC' crossorigin='anonymous'>
                
                    <title>Hello, world!</title>
                    </head>
                    <body>
                    <h1>Pngajuan Klaim/Penyelesaian Advance </h1></br>
                    <b><p>@hasil</p></b>
                    <table style='border:0px !important'>
                        <tr>
                            <td>Nomor Advance</td>
                            <td>:</td>
                            <td>@nomor</td>
                        </tr>
                        <tr>
                            <td>Nama</td>
                            <td>:</td>
                            <td>@nama</td>
                        </tr>
                        <tr>
                            <td>Keperluan</td>
                            <td>:</td>
                            <td>@keperluan</td>
                        </tr>
                        <tr>
                            <td>Nominal Pengajuan</td>
                            <td>:</td>
                            <td>@nominal</td>
                        </tr>
                        <tr>
                            <td>Nominal Penyelesaian/Klaim</td>
                            <td>:</td>
                            <td>@amount</td>
                        </tr>
                    </table>
                    <hr>
                    <p>Dokument ini membutuhkan verifikasi dari anda, silahkan klik <a href='@link'><b>Periksa</b></a> untuk melihat pengajuan lebih detail</p>
                    <hr>
                    <button style='color:white; background-color: cornflowerblue; border-radius: 5px;'>Periksa</button>
                    <script src='https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js' integrity='sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM' crossorigin='anonymous'></script>
                    </body>
                </html>";
        return $temp;
    }
    function templateRespone(){
        $temp = "<!doctype html>
        <html lang='en'>
            <head>
            <meta charset='utf-8'>
            <meta name='viewport' content='width=device-width, initial-scale=1'>
        
            <link href='https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css' rel='stylesheet' integrity='sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3' crossorigin='anonymous'>
            </head>
            <style>
                tr, td {
                    padding: 10px;
                }
            </style>
            <body>
            <h1>Konfirmasi Advance</h1>
            <br>
            <p>
                Melalui email ini kami memberitahukan kepada anda bahwa, <br>
                pengajuan advance dengan nomor <b>@nomor</b> untuk keperluan <b>@keperluan</b> 
            </p>
            <br>
            <h3><b>@status</b></h3>
            <p>@catatan</p>
            <br>
            Silahkan akses <a href='employee.ama.id'><b>Disini</b></a> untuk melihat detail serta update pengerjaan nya
            </body>
        </html>";

        return $temp;
    }
    function templateEmailFeedBack(){
        $temp = "<!doctype html>
        <html lang='en'>
          <head>
            <meta charset='utf-8'>
            <meta name='viewport' content='width=device-width, initial-scale=1'>
        
            <link href='https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css' rel='stylesheet' integrity='sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3' crossorigin='anonymous'>
          </head>
           <style>
                tr, td {
                    padding: 10px;
                }
            </style>
          <body>
            <h1>@judul</h1>
            <br>
            <p>
                Melalui email ini kami memberitahukan kepada anda bahwa, <br>
                <b>@pengajuan</b><br>
                <b>@isi</b> 
            </p>
            <br>
            Silahkan akses <a href='employee.ama.id'><b>Disini</b></a> untuk melihat informasi detail
          </body>
        </html>";

        return $temp;
    }
    function templateEmailWoOke(){
        $temp = "<!doctype html>
        <html lang='en'>
          <head>
            <meta charset='utf-8'>
            <meta name='viewport' content='width=device-width, initial-scale=1'>
        
            <link href='https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css' rel='stylesheet' integrity='sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3' crossorigin='anonymous'>
          </head>
           <style>
                tr, td {
                    padding: 10px;
                }
            </style>
          <body>
            <h1>Konfirmasi Work Order</h1>
            <br>
            <p>
                Melalui email ini kami memberitahukan kepada anda bahwa, <br>
                <b>Work Order</b> kamu untuk <b>@nama</b> sudah di balas dan di konfirmasi pada <b>@tgl</b> :
            </p>
            <br>
            <script src='https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js' integrity='sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p' crossorigin='anonymous'></script>
            <table>
                <tbody>
                    <tr >
                        <td style='width:200px'>Anda</td>
                        <td>:</td>
                        <td style='width:700px'>@anda</td>
                    </tr>
                    <tr style='background-color:#efefef'>
                        <td>@nama</td>
                        <td>:</td>
                        <td>@isi</td>  
                    </tr>
                </tbody>
            </table>
            <br>
            Silahkan akses <a href='employee.ama.id'><b>Disini</b></a> untuk melihat detail serta update pengerjaan nya
          </body>
        </html>";

        return $temp;
    }
    function templateEmailSakit(){
        $temp = "<!doctype html>
        <html lang='en'>
          <head>
            <meta charset='utf-8'>
            <meta name='viewport' content='width=device-width, initial-scale=1'>
        
            <link href='https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css' rel='stylesheet' integrity='sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3' crossorigin='anonymous'>
          </head>
           <style>
                tr, td {
                    padding: 10px;
                }
            </style>
          <body>
            <h1>@jenis</h1>
            <script src='https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js' integrity='sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p' crossorigin='anonymous'></script>
            <table>
                <tbody>
                    <tr >
                        <td style='width:200px'>Nama</td>
                        <td>:</td>
                        <td style='width:500px'>@nama</td>
                    </tr>
                    <tr style='background-color:#efefef'>
                        <td>Dari</td>
                        <td>:</td>
                        <td>@tgl1</td>
                    </tr>
                    <tr>
                        <td>Sampai</td>
                        <td>:</td>
                        <td>@tgl2</td>
                    </tr>
                    <tr style='background-color:#efefef'>
                        <td>Keterangan</td>
                        <td>:</td>
                        <td>@keterangan</td>
                    </tr>
                </tbody>
            </table>
            <br>
            <p>
                
            </p>
           </body>
        </html>";
        return $temp;
    }
    function templateEmail(){
        $temp = "<!doctype html>
        <html lang='en'>
          <head>
            <meta charset='utf-8'>
            <meta name='viewport' content='width=device-width, initial-scale=1'>
        
            <link href='https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css' rel='stylesheet' integrity='sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3' crossorigin='anonymous'>
          </head>
           <style>
                tr, td {
                    padding: 10px;
                }
            </style>
          <body>
            <h1>@jenis</h1>
            <script src='https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js' integrity='sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p' crossorigin='anonymous'></script>
            <table>
                <tbody>
                    <tr >
                        <td style='width:200px'>Nama</td>
                        <td>:</td>
                        <td style='width:500px'>@nama</td>
                    </tr>
                    <tr style='background-color:#efefef'>
                        <td>Dari</td>
                        <td>:</td>
                        <td>@tgl1</td>
                    </tr>
                    <tr>
                        <td>Sampai</td>
                        <td>:</td>
                        <td>@tgl2</td>
                    </tr>
                    <tr style='background-color:#efefef'>
                        <td>Keterangan</td>
                        <td>:</td>
                        <td>@keterangan</td>
                    </tr>
                </tbody>
            </table>
            <br>
            <p>
                
            </p>
           <a href='@link'> <Button class='btn btn-success float-left' style='background-color:#2896e5; color:#ffff'>Approve</button></a>
           <a href='@reject'> <Button class='btn btn-danger float-right' style='background-color:#d92121; color:#ffff'>Reject</button></a>
          </body>
        </html>";
        return $temp;
    }
    function templateEmailWo(){
        $temp = "<!doctype html>
        <html lang='en'>
          <head>
            <meta charset='utf-8'>
            <meta name='viewport' content='width=device-width, initial-scale=1'>
        
            <link href='https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css' rel='stylesheet' integrity='sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3' crossorigin='anonymous'>
          </head>
           <style>
                tr, td {
                    padding: 10px;
                }
            </style>
          <body>
            <h1>@jenis</h1>
            <br>
            <p>
                Melalui email ini kami memberitahukan kepada anda bahwa, anda mendapatkan <br>
                <b>Work Order</b>, dengan detail sebagai berikut :
            </p>
            <br>
            <script src='https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js' integrity='sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p' crossorigin='anonymous'></script>
            <table>
                <tbody>
                    <tr >
                        <td style='width:500px'>Nama Pembuat WO</td>
                        <td>:</td>
                        <td style='width:500px'>@nama</td>
                    </tr>
                    <tr style='background-color:#efefef'>
                        <td>Expected</td>
                        <td>:</td>
                        <td>@tgl1</td>  
                    </tr>
                    <tr >
                        <td>Keterangan</td>
                        <td>:</td>
                        <td>@keterangan</td>
                    </tr>
                </tbody>
            </table>
            <br>
            Silahkan akses <a href='employee.ama.id'><b>Disini</b></a> untuk melihat detail serta update pengerjaan nya
          </body>
        </html>";

        return $temp;
    }
    function templateEmailLembur(){
        $temp = "<!doctype html>
        <html lang='en'>
          <head>
            <meta charset='utf-8'>
            <meta name='viewport' content='width=device-width, initial-scale=1'>
        
            <link href='https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css' rel='stylesheet' integrity='sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3' crossorigin='anonymous'>
          </head>
           <style>
                tr, td {
                    padding: 10px;
                }
            </style>
          <body>
            <h1>Pengajuan Lembur</h1>
            <br>
            <p>
                Melalui email ini kami memberitahukan kepada anda bahwa ada Pengajuan Lembur dari :
            </p>
            <br>
            <script src='https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js' integrity='sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p' crossorigin='anonymous'></script>
            <table>
                <tbody>
                    <tr style='background-color:#efefef'>
                        <td style='width:500px'>Nama Pembuat</td>
                        <td>:</td>
                        <td style='width:500px'>@nama</td>
                    </tr>
                    <tr>
                        <td style='width:500px'>Department</td>
                        <td>:</td>
                        <td style='width:500px'>@dept</td>
                    </tr>
                    <tr style='background-color:#efefef'>
                        <td>Jumlah Jam</td>
                        <td>:</td>
                        <td>@jam</td>  
                    </tr>
                    <tr >
                        <td>Periode</td>
                        <td>:</td>
                        <td>@periode</td>
                    </tr>
                    <tr style='background-color:#efefef'>
                        <td>Keterangan</td>
                        <td>:</td>
                        <td>@keterangan</td>
                    </tr>
                </tbody>
            </table>
            <hr>
            <b>Nama Karyawan</b>
            @karyawan
            <br>
            Silahkan akses <a href='employee.ama.id'><b>Disini</b></a> untuk melihat detail serta update pengerjaan nya
          </body>
        </html>";

        return $temp;
    }
    function templateEmailAdvance(){
        $temp = "<!doctype html>
        <html lang='en'>
          <head>
            <meta charset='utf-8'>
            <meta name='viewport' content='width=device-width, initial-scale=1'>
        
            <link href='https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css' rel='stylesheet' integrity='sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3' crossorigin='anonymous'>
          </head>
           <style>
                tr, td {
                    padding: 10px;
                }
            </style>
          <body>
            <h1>@judul</h1>
            <br>
            <p>
                Melalui email ini kami memberitahukan kepada anda bahwa pada @tgl ada Pengajuan Advance dari :
            </p>
            <br>
            <script src='https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js' integrity='sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p' crossorigin='anonymous'></script>
            <table style='margin-top:-10px'>
                <tbody>
                    <tr style='background-color:#efefef'>
                        <td style='width:500px'>Nama Pembuat</td>
                        <td>:</td>
                        <td style='width:500px'>@nama</td>
                    </tr>
                    <tr>
                        <td style='width:500px'>Department</td>
                        <td>:</td>
                        <td style='width:500px'>@dept</td>
                    </tr>
                    <tr style='background-color:#efefef'>
                        <td>Biaya Estimasi</td>
                        <td>:</td>
                        <td>@amount</td>  
                    </tr>
                    <tr >
                      <td>Keterangan</td>
                      <td>:</td>
                      <td>@keterangan</td>
                    </tr>
                </tbody>
            </table>
            <hr>
            <b>Detail Keperluan</b><br>
            @detail
            <br>
            Silahkan akses <a href='@target'><b>Disini</b></a> untuk melihat detail serta update pengerjaan nya
          </body>
    </html>";
        return $temp;
    }

    function queryJson($con,$qr){
        error_reporting(0);
        ini_set('display_errors', 0);
        $res = mysqli_query($con,$qr);
        // echo $qr;
        if(mysqli_num_rows($res) > 0){
            while($da = mysqli_fetch_array($res)){
                $row [] = $da;
            }
            $obj = new stdClass();
            $obj -> data = $row;
            $data = json_encode($obj);
            return $data;
        }else{
            $obj = new stdClass();
            $obj -> data = [];
            $data = json_encode($obj);
            return $data;
        }
    }
    function queryJsonMenu($con,$qr){
        // error_reporting(0);
        // ini_set('display_errors', 0);

        $res = mysqli_query($con,$qr);
        if(mysqli_num_rows($res) > 0){
            $include = "'xx'";
            while($da = mysqli_fetch_array($res)){
                $row [] = $da;
                // if(strlen($da['has_child']) > 0){
                    $include = $include.",'".$da['menu_header']."'";
                // }
            }

            $selp = "select * from m_menu where menu_header in ($include) and has_child is not null";
            $resp = mysqli_query($con,$selp);

            while ($dt = mysqli_fetch_array($resp)) {
                $rowz [] = $dt;
                // echo $dt['icon'];
            }
            $obj = new stdClass();
            $obj -> data = $row;
            $obj -> data_child = $rowz ? $rowz : [];
            $data = json_encode($obj);

            return $data;
        }else{
            $obj = new stdClass();
            $obj -> data = [];
            $data = json_encode($obj);
            return $data;
        }
    }
    // function getCountTable($tbl,$con){
        // $data = 0;
        // $qr = "select count(*)+1 as cnt,uuid() as id from transaksi_point where m_agent_id = '$tbl'";
        // $res = mysqli_query($con,$qr);
        // $dr = mysqli_fetch_array($res);
        // $data = $dr;
    //     return 0; //$data;


    // }

    // function transApprove($con){
    //     $data = 0;
    //     $qr = "select count(*)+1 as cnt,uuid() as id from transaksi_agent";
    //     $res = mysqli_query($con,$qr);
    //     $dr = mysqli_fetch_array($res);
    //     $data = $dr['cnt'];
    //     return sprintf("%05s", $data);
    // }

    function notice($pesan){
        return "<script>alert('$pesan')</script>";
    }
    function uniq(){
        return (rand(1,1000000000000)) ;
    }

    function getcutoff($con){
        $nw = date("d");
        // $nw = date_create("2013-03-15");
        $sel = "select * from cut_off";
        $res = mysqli_query($con,$sel);
        $dt = mysqli_fetch_array($res);
        $tgl = $dt['periode']; //tgl 16
        $tgl2 = $dt['periode_awal']; //tgl 01
        $per = $dt['periode'];
        $per2 = $dt['periode_awal'];
        if($nw >= $tgl){
            $nxtm = strtotime("next month");
            $tgl = date("Y-m", $nxtm);
            $tgl = $tgl."-0".$per2;
        }else{
            $tgl = date("Y-m");
            $tgl = $tgl."-".$per;
        }
        return $tgl;
    }
    
    function ceknamabarang($con,$id){
        $sel = "select * from m_barang where m_barang_id = '$id'";
        $dt = mysqli_fetch_array(mysqli_query($con,$sel));
        return $dt['nama_barang'];
    }

    function cekbarangpo($con,$qr){
        $sel = $qr;
        $dt = mysqli_fetch_array(mysqli_query($con,$sel));
        return $dt['qty'];
    }
    function cekUrutanAgent($con){
        $sel = "select count(*)+1 as number from jemaah";
        $dt = mysqli_fetch_array(mysqli_query($con,$sel));
        return "JB".sprintf("%04s", $dt['number']);
    }

    function cekUrutanResi($con){
        $sel = "select count(*)+1 as number from jemaah";
        $dt = mysqli_fetch_array(mysqli_query($con,$sel));
        return "JB".sprintf("%08s", $dt['number']);
    }
    function idwisatawan($con,$id,$nomor){
        $sel = "select count(*)+1 as number from jemaah where m_agent_id = '$id'";
        $dt = mysqli_fetch_array(mysqli_query($con,$sel));
        $sel_agent = "select SUBSTR(nomor_agent,4,4) as key_ from m_agent where m_agent_id = '$id'";
        $dt1 = mysqli_fetch_array(mysqli_query($con,$sel_agent));

        return "JU".$nomor."-".sprintf("%04s", $dt['number']);
    }

    function cekdetailpo($con,$qr){
        
        $sel = "select count(*) as jml from t_po_detail where t_po_id = '$qr'";
        echo $sel;
        // $dt = mysqli_fetch_array(mysqli_query($con,$sel));
        // return $dt['jml'];
    }
    function urutanKlaim($numb){
        return sprintf("%04s", $numb);
    }

    function cekstatusPO($con,$nopo){
        $sel0 = "select sum(cast(qty_gr as float))as qty from t_gr a
        inner join t_gr_detail b on a.t_gr_id = b.t_gr_id
        where t_po_id = '$nopo'";
        $res = mysqli_query($con,$sel0);
        $dt0 = mysqli_fetch_array($res);
        $qtyGR  = $dt0['qty'];


        $sel1 = "select sum(cast(qty as float)) as qty_po from t_po a
        inner join t_po_detail b on a.t_po_id = b.t_po_id
        where a.t_po_id = '$nopo'";
        $res = mysqli_query($con,$sel1);
        $dt1 = mysqli_fetch_array($res);
        $qtyPO  = $dt1['qty_po'];
        if((float) $qtyGR < (float) $qtyPO){
            $status = "OUTSTANDING";
        }else{
            $status = "COMPLETE";
        }

        return $status;
    }

    function querytodataset($con,$seq){
        $res = mysqli_query($con,$seq);
        $dt1 = mysqli_fetch_array($res);
        // echo $dt1['t_mutasi_id'];
        return $dt1;
    }
    function console($log){
        // $log = "ss";
        echo "<script>console.log('$log');</script>";
        // echo $log;
    }
    
    function cekSisa($con,$kode,$batch,$rak,$jml){
        $data = 0;
        $qr = "select last_stok - $jml as sisa from t_stok 
        where m_barang_id = '$kode' and batch = '$batch' and m_rak_id = '$rak'";
        $res = mysqli_query($con,$qr);
        $dr = mysqli_fetch_array($res);
        $data = $dr['sisa'];
        return $data;
    }

    function nomorGR($con,$tipe){
        $data = 0;
        $qr = "select count(*)+1 as jml from t_gr 
        where CONCAT(year(createdate),month(createdate)) = CONCAT(year(now()),month(now()))
        and tipe_gr = '$tipe'";
        $res = mysqli_query($con,$qr);
        $dr = mysqli_fetch_array($res);
        $count = $dr['jml'];

        $var_depan = "";
        if($tipe == "Purchase Order"){
            $var_depan = "WH-VCM";
        }elseif($tipe == "Gudang Sample"){
            $var_depan = "WH-SPL";
        }elseif($tipe == "Retur Customer"){
            $var_depan = "WH-RTRC";
        }else{
            $var_depan = "?";
        }
        $month = date('m');
        $year = date('yy');
        $month = romanNumber($month);
        // $month = date("F", mktime(0, 0, 0, $month, 10));
        $month = strtoupper(substr($month,0,3));

        $data = sprintf("%05d",$count)."/".$var_depan."/".$month."/".$year;
        

        return $data;
    }
    function runningnumber($count,$var_depan){
        $month = date('m');
        $year = date('yy');
        $month = romanNumber($month);
        // $month = date("F", mktime(0, 0, 0, $month, 10));
        $month = strtoupper(substr($month,0,3));

        $data = $var_depan."/".$month."/".$year."/".sprintf("%03d",$count);
        

        return $data;
    }
    function romanNumber($numb){
        $no = 0;
        switch ($numb){
            case "01":
                $no = "I";
            break;
            case "02":
                $no = "II";
            break;
            case "03":
                $no = "III";
            break;
            case "04":
                $no = "IV";
            break;
            case "05":
                $no = "V";
            break;
            case "06":
                $no = "VI";
            break;
            case "07":
                $no = "VII";
            break;
            case "08":
                $no = "VIII";
            break;
            case "09":
                $no = "IX";
            break;
            case "10":
                $no = "X";
            break;
            case "11":
                $no = "XI";
            break;
            case "11":
                $no = "XII";
            break;
            default : 
            $no = "0";
            break;
        }
        return $no;
    }
    function rupiah($angka){
	
        $hasil_rupiah = "Rp " . number_format($angka,2,',','.');
        return $hasil_rupiah;
     
    }

    function format($v){
        $res = "";
        if (strpos($v,'.')){
            $res = number_format($v,2);
        }else{
            $res = $v;
        }

        return $res;
    }

?>