<?php
    include('config/config.php');

    if(isset($_GET['go'])){
        $go = $_GET['go'];
        if($go == "in"){
            include('pages/transaksimasuk/transaksimasuk.php');
        }else if($go == "out"){
            include('pages/transaksikeluar/transaksikeluar.php');
        }else if($go == "inputitem"){
            include('pages/master/inputitem.php');
        }else if($go == "items"){
            include('pages/master/masteritem.php');
        }else if($go == "balance"){
            include('pages/balance/balance.php');
        }else if($go == "laporan"){
            include('pages/laporan/laporan.php');
        }
    }
?>
